//Теория
// 1 Опишіть своїми словами що таке Document Object Model (DOM)
// Это дерево, которое содержит видимые объекты и текст с системой вложенности в браузере

// 2 Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML - удаляет все содержимое єлемента и заменяет фрагментом HTML с тегами. Не безопасно использовать.
// innerText - позволяет создавать текст. Имеет свойство чтения тега. Так же позволяет получить CSS

// 3 Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Самые универсальные
// document. querySelector("") поиск одного элемента
// document. querySelectorAll("") поиск всех элементов. Возвращает коллекцию, псевдоклассы тоже поддерживаются
// document.getElementById() поиск id
// Так же существуют
// getElementsByName () поиск по имени
// getElementsByTagName() по тегу
// getElementsByClassName() по классу

// Задание
// Код для завдань лежить в папці project.
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphAll = document.querySelectorAll("p");
console.log(paragraphAll);
paragraphAll.forEach((i) => (i.style.background = "#FF0000"));

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const byId = document.getElementById("optionsList");
console.log(byId);
console.log(byId.parentNode);

if (byId.hasChildNodes()) {
  console.log(byId.childNodes);
}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

// const testPar = document.createElement("p");
// testPar.classList.add("testParagraph");

const testPar = document.getElementById("testParagraph");
testPar.innerText = "This is a paragraph";
console.log(testPar);

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector(".main-header");
console.log(mainHeader.children);
[...mainHeader.children].forEach((i) => {
  i.classList.add("nav-item");
});

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = document.querySelectorAll(".section-title");
console.log(sectionTitle);
sectionTitle.forEach((i) => i.remove());
